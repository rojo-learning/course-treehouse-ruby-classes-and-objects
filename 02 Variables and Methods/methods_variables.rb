
### Variables & Methods

## Instance Variables
# These variables conserve the state of values for all the life of the object

class Name
  def initialize(title, first_name, middle_name, last_name)
    # title is a local variable, only visible for this method
    # @title is an instance variable, visible for all the methods of the object
    @title       = title
    @first_name  = first_name
    @middle_name = middle_name
    @last_name   = last_name
  end

  def title
    @title
  end

  def first_name
    @first_name
  end

  def middle_name
    @middle_name
  end

  def last_name
    @last_name
  end
end

puts 'Name 1 -----'
name = Name.new('Mr.', 'David', 'O', 'Rojo')
puts name.title + ' ' + name.first_name + ' ' +
     name.middle_name + ' ' + name.last_name

## Attribute Readers
# Shortcuts to create methods to access variables from outside a class

class Name2
  attr_reader :title, :first_name, :middle_name, :last_name

  def initialize(title, first_name, middle_name, last_name)
    @title       = title
    @first_name  = first_name
    @middle_name = middle_name
    @last_name   = last_name
  end
end

puts 'Name 2 -----'
name2 = Name2.new('Mr.', 'Juan', 'J', 'Bustamante')
puts name2.title + ' ' + name2.first_name + ' ' +
     name2.middle_name + ' ' + name2.last_name


## Setters, Attribute Writers and Attribute Accessors

class Name3
  attr_accessor :last_name
  attr_reader :title
  attr_writer :title

  # The two methods below are the equivalent to define one attr_accessor
  def first_name=(name)
    # This method is the equivalent to one attr_writer
    @name = name
  end

  def first_name
    # This method is the equivalent to one attr_reader
    @name
  end
end

puts 'Name 3 -----'
name3 = Name3.new
name3.first_name = 'Luz'
name3.last_name  = 'Aranda'

puts name3.first_name + ' ' + name3.last_name

## Methods
# Allow to perform sets of operations with the data of the objects/classes
class Name4
  attr_accessor :title, :first_name, :middle_name, :last_name

  def initialize(title, first_name, middle_name, last_name)
    @title       = title
    @first_name  = first_name
    @middle_name = middle_name
    @last_name   = last_name
  end

  def full_name
    # We combine the value of several variables with this method
    @first_name + ' ' + @middle_name + ' ' + @last_name
  end

  def full_name_with_title
    # We can call other methods inside methods
    @title + ' ' + full_name()
  end

end

puts 'Name 4 -----'
name4 = Name4.new('Mr.', 'Luis', 'A', 'Solis')
puts name4.full_name_with_title()

## to_s method

class Name4
  # We can reopen a class to add or modify methods
  def to_s
    # this method is called by the puts and print methods
    full_name_with_title()
  end
end

puts 'to_s -----'
puts name4
