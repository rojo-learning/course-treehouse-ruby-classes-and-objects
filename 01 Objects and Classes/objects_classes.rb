
### Classes

## Class Instantiation
string = String.new('David')
array  = Array.new

## Class Definition
class MyClass
  def initialize
    puts 'The #initialize method is called when the class is instantiated.'
  end
end

MyClass.new

# Check the methods than an object supports
p string.methods

# Check if an object responds to a specific method
p string.respond_to?('upcase')

# Creating a class with methods
class Name
  def title
    "Mr."
  end

  def first_name
    "David"
  end

  def middle_name
    "O"
  end

  def last_name
    "Rojo"
  end
end

name = Name.new

puts name.title()
puts name.first_name()
puts name.middle_name()
puts name.last_name()
