
class BankAccount
  attr_reader :name

  def initialize(name)
    @name = name
    @transactions = []
    add_transaction "Beginning Balance", 0
  end

  def add_transaction(description, amount)
    @transactions.push(description: description, amount: amount)
  end

  def balance
    @transactions.map { |transac| transac[:amount] }.reduce(0,:+)
  end

  def credit(description, amount)
    add_transaction description, amount
  end

  def debit(description, amount)
    add_transaction description, -amount
  end

  def register
    register = "#{name}'s Bank Account\n"
    register += "-" * 40 + "\n"

    register += "Description".ljust(30) + "Amount".rjust(10) + "\n"
    @transactions.each do |transac|
      register += transac[:description].ljust(30) +
        sprintf('%0.2f', transac[:amount]).rjust(10) + "\n"
    end

    register += "-" * 40 + "\n"
    register += "Balance:".ljust(30) + sprintf('%0.2f', balance).rjust(10) + "\n"
    register += "-" * 40 + "\n"
  end

  def print_register
    puts register
  end

  def to_s
    "Name: #{name}, Balance: #{sprintf('%0.2f', balance)}"
  end

end
