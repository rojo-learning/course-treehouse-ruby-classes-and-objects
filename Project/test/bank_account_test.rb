
require 'minitest/autorun'
require_relative '../lib/bank_account.rb'

describe BankAccount do
  let(:holder ) { 'John Down'             }
  let(:balance) { 0.0                     }
  let(:account) { BankAccount.new(holder) }

  describe 'class' do
    it 'instance is a BankAccount' do
      account.must_be_instance_of BankAccount
    end

    it "provides reader to holder's name" do
      account.must_respond_to :name
    end

    it 'sets the provided holder name' do
      account.name.must_equal holder
    end

    it 'has a starting balance of 0' do
      account.balance.must_equal 0
    end
  end

  describe '#add_transaction' do
    it 'accepts 2 parameters: description and amount' do
      description = 'cash deposit'
      amount = 100

      proc { account.add_transaction(description, amount) }.must_be_silent
    end
  end

  describe '#balance' do
    before do
      account = BankAccount.new '#balance Test Account Holder'
    end

    it 'returns the correct balance' do
      amount = 100
      account.credit('test credit', amount)

      account.balance.must_equal amount
    end
  end

  describe '#credit' do
    it 'accepts description and amount as parameters' do
      description = 'cash deposit'
      amount = 100

      proc { account.credit(description, amount) }.must_be_silent
    end
  end

  describe '#debit' do
    it 'accepts 2 parameters: description and amount' do
      description = 'cash deposit'
      amount = 100

      proc { account.debit(description, amount) }.must_be_silent
    end
  end

  describe '#register' do
    let(:credit_description) { 'Payment Check'               }
    let(:credit_amount     ) { 100                           }
    let(:debit_description ) { 'Scholarship'                 }
    let(:debit_amount      ) { 80                            }
    let(:expected_balance  ) { credit_amount - debit_amount  }

    it "shows the account holder's name" do
      account.register.must_match /#{holder}'s Bank Account/
    end

    it 'shows credits operation on the register' do
      account.credit credit_description, credit_amount

      account.register.must_match /#{credit_description}/
      account.register.must_match /#{sprintf('%0.2f', credit_amount)}/
    end

    it 'shows debits operation on the register' do
      account.debit  debit_description, debit_amount

      account.register.must_match /#{debit_description}/
      account.register.must_match /#{sprintf('%0.2f', debit_amount)}/
    end

    it 'shows the correct account balance' do
      account.credit credit_description, credit_amount
      account.debit  debit_description, debit_amount

      account.register.must_match /#{sprintf('%0.2f', expected_balance)}/
    end
  end

  describe '#to_s' do
    it "shows the account holder's name" do
      account.to_s.must_match /Name: #{holder}/
    end

    it "shows the account balance" do
      account.to_s.must_match /Balance: #{balance.to_s}/
    end
  end

end
