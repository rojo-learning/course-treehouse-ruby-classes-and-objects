
require_relative('lib/bank_account.rb')

account = BankAccount.new('David Rojo')
account.credit 'Paycheck', 100
account.debit  'Groceries', 40
account.debit  'Gas', 10.51

puts account

puts "Register:"
account.print_register
