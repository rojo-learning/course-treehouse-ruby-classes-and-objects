
# Treehouse - Ruby Objects and Classes
Exercises and examples from the 'Ruby Objects and Classes' course, imparted by Jason
Seifer at [Treehouse](http://teamtreehouse.com/).

> Ruby is known as an "Object Oriented" programming language. But what does object oriented mean? In this course, we'll cover the basics of Ruby Classes. We'll learn what classes are, how they are used, and how to write our own.

## Topics
- *Ruby Objects and Classes*: Objects and Classes are the blueprints and building blocks of the Ruby language. In this stage, we'll learn what classes are, where objects come from, and write our own class.

- *Variables and Methods*: Classes and objects become much more useful when we keep our program's logic in them. In this stage, we'll learn how to use variables in our classes and where variables can and can't be used. We'll also learn to write methods in our classes, which make them more robust.

## Project
- *Build a Bank Account Class*: Using our knowledge of writing classes, we're going to create a simple bank account class. The class will keep track of whom it belongs to as well as the account's transactions and its ongoing balance.


---
This repository contains portions of code from the TreehouseTeam courses and is included only for reference under _fair use_.
